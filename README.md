# My SSH reminder

## Removing a key from known hosts:

    ssh-keygen -f "/home/user/.ssh/known_hosts" -R "key_name"

## Creating a key:

    ssh-keygen -o -a 100 -t ed25519 -C user@email #create an ED25519 key
    ssh-keygen -b 4096 -t rsa -C user@email #create a RSA key
    ssh-add newkey #add newkey to your SSH agent
    ssh-add *
    ssh-add .

Don't use `sudo` for your own keys. Keys must have the 700 permissions to be successfully added. In CentOS 8, add the keys one by one from your `~/.ssh` directory.

## Listing existing keys:

    ssh-add -l

## Displaying a key:

    cat /home/user/.ssh/key_name

## Displaying a key fingerprint (with random art image):

    ssh-keygen -lvf key_name

## Permissions :

When copying a key, if `ssh-add` displays the following error :

    Permissions 0640 for 'id_rsa' are too open.
    It is required that your private key files are NOT accessible by others.
    This private key will be ignored.

it is required to change the key perms to 400 or 600.

## Enabling SSH agent:

    eval $(ssh-agent -s)
    eval $(ssh-agent)

## Automatically starting ssh-agent and adding SSH keys

By default, CentOS 8 doesn't keep the memory of SSH keys after reboot.

Once the keys are copied into `~/.ssh`, a few lines must be added to `.bashrc` or `.zshrc`:
```
if [ -z "$SSH_AUTH_SOCK" ] ; then
 eval `ssh-agent -s`
 ssh-add
fi
```

## ssh-add options

```
ssh-add -D # Delete all identities
ssh-add -d Idfile
ssh-add -l #List all identities
ssh-add IDfile
ssh-add .ssh/id_rsa*
```

## Copying a key to a distant server:

    ssh-copy-id user@server #Copy all keys that are registered in SSH agent
    ssh-copy-id -i newkey.pub user@server\n #Copy newkey.pub

## Removing a key from authorized keys:

Removing a key from a server can be done by editing your `/home/user/.ssh/authorized_keys` file.

## Removing a key from known hosts:

    ssh-keygen -f "/home/user/.ssh/known_hosts" -R "server_address"    

## Connecting and copying from a different port number:

    ssh -p port user@server
    scp -P port file user@server:path/
    rsync -e ssh -avz /source server:/target #with compression (z)
    rsync -e 'ssh -p <port>' -avz /source server:/target
    rsync -ave ssh server:/source /target
    ssh-copy-id -i newkey.pub -p port user@server

## Connecting a distant server in your file browser under Linux using SFTP:

Type the adress:

    ssh://user@server:port


## Sources

- https://delicious-insights.com/fr/articles/comprendre-et-maitriser-les-cles-s

*See also:* [Enabling a SSH key on a Synology NAS running DSM 6](https://gitlab.com/sakura-lain/ssh-synology-nas-dsm-6/)
